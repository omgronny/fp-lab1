import Data.Char
import Data.Foldable

factorial :: Integer -> Integer
factorial 0 = 1
factorial n = n * factorial (n - 1)

lenVec3 x y z = sqrt (x^2 + y^2 + z^2)

sign n
  | n > 0 = 1
  | n < 0 = -1
  | otherwise = 0

x |-| y
  | x > y = x - y
  | otherwise = y - x


twoDigits2Int x y
  | isDigit x && isDigit y = (ord x - 48) * 10 + (ord y - 48)
  | otherwise = 100

doubleFact n
  | n <= 0 = 1
  | otherwise = n * doubleFact (n - 2)

fibHelper :: Integer -> Integer -> Integer -> Integer
fibHelper f_nm2 f_nm1 n
  | n == 2    = f_nm2 + f_nm1
  | otherwise = fibHelper f_nm1 (f_nm1 + f_nm2) (n - 1)

fibLinear :: Integer -> Integer
fibLinear n
  | n == 0    = 0
  | n == 1    = 1
  | otherwise = fibHelper 0 1 n

main = print $ foldr (+) 0 [1,2,3,4,5]
